/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   triggers.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/24 17:37:17 by bcherkas          #+#    #+#             */
/*   Updated: 2019/03/01 18:48:45 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static int	move(int key, t_info *inf)
{
	double const	el = ABS((inf->mlb.max_y - inf->mlb.min_y) / 20.0);
	double			y;
	double			x;

	x = 0;
	y = 0;
	if (key == 123)
		x = el;
	else if (key == 124)
		x = -el;
	else if (key == 125)
		y = el;
	else if (key == 126)
		y = -el;
	inf->mlb.max_y += y;
	inf->mlb.min_y += y;
	inf->mlb.max_x += x;
	inf->mlb.min_x += x;
	draw_function(inf);
	return (1);
}

static int	reset(t_info *inf)
{
	inf->wrap_func(inf);
	return (1);
}

static int	change_iter(int key, t_info *inf)
{
	if (key == 78 && inf->mlb.max_iter < 30)
		return (1);
	inf->mlb.max_iter += (key == 78) ? -5 : 5;
	draw_function(inf);
	return (1);
}

static int	fix_pos(t_info *inf)
{
	if (inf->draw_func != julia && inf->draw_func != julia_ext)
		return (0);
	inf->fixed ^= 1;
	return (1);
}

int			triggers(int key, void *elem)
{
	t_info *const	inf = (t_info *)elem;

	if (key == 53)
		return (escapewindow(inf));
	else if (key == 123 || key == 124 || key == 125 || key == 126)
		return (move(key, inf));
	else if (key == 15)
		return (reset(inf));
	else if (key == 78 || key == 69)
		return (change_iter(key, inf));
	else if (key == 3)
		return (fix_pos(inf));
	else
		return (triggers2(key, inf));
}
