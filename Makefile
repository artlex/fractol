# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/04/24 17:06:27 by bcherkas          #+#    #+#              #
#    Updated: 2019/03/01 19:11:03 by bcherkas         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

.PHONY: all clean fclean re

CC=gcc
CFLAGS=-Wall -Wextra -Werror -I./libft -I./include -o3

MLX=-I/usr/local/include -L/usr/local/lib -lmlx -framework OpenGL -framework AppKit

LIBFT=./libft/libft.a

SETS=julia.c julia2.c mandelbrot.c tricorn.c heart_mandelbrot.c perpendic_mandel.c burning_ship.c celtic.c
SRC= $(SETS) fractol.c triggers.c triggers2.c escapewindow.c color.c help.c draw_function.c mouse_events.c colors2.c
OBJ=$(SRC:.c=.o)
INC=fractol.h

SRCDIR=src
OBJDIR=obj
INCDIR=include

NAME=fractol

all: $(OBJDIR) $(NAME)

$(OBJDIR):
	mkdir -p $@

$(NAME): $(addprefix $(OBJDIR)/, $(OBJ)) $(LIBFT)
	$(CC) $(CFLAGS) $(MLX) -o $@ $^
	@echo "Fractol compiled"

$(addprefix $(OBJDIR)/, %.o): $(addprefix $(SRCDIR)/, %.c) $(addprefix $(INCDIR)/, $(INC))
	$(CC) $(CFLAGS) -c -o $@ -pthread $<

$(LIBFT):
	@$(MAKE) -C ./libft
	@echo "libft compiled"

clean:
	@rm -rf $(OBJDIR)
	@$(MAKE) -C ./libft clean
	@echo "clean done"

fclean: clean
	@rm -f $(NAME)
	@$(MAKE) -C ./libft fclean
	@echo "fclean done"
re: fclean all
